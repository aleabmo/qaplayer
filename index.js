var express = require('express');
var app = express();
var path = require('path');

//Index per tal de poder servir una llista de diferents records
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'weo.html'));
});

//Example de com funciona
app.get('/wea.html', function(req, res) {
    res.sendFile(path.join(__dirname, 'wea.html'));
});

/***
    Aquestes dues serveixen el js i el css per tal de renderitzar els events
    ***/
app.get('/dist/rrweb.min.js', function(req, res) {
    res.sendFile(path.join(__dirname, '/dist/rrweb.min.js'));
});

app.get('/dist/rrweb.min.css', function(req, res) {
    res.sendFile(path.join(__dirname, '/dist/rrweb.min.css'));
});

app.listen(3000, function() {
    console.log("Miteeel server-up on port 3000");
});
